@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <div class="col-10">   
                            Employee
                        </div>
                        <div class="col-2 text-center">
                            <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
                        </div>
                    </div>
                </div>

                {{-- create new employee --}}
                <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                    name="name" value="{{ $employee->name }}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control"
                                    name="phone" value="{{ $employee->phone }}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address"
                                class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea name="address"
                                class="form-control" disabled>{{ $employee->address }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="department"
                                class="col-md-4 col-form-label text-md-right">{{ __('Department') }}</label>

                            <div class="col-md-6">
                                <input id="deparment" type="text" class="form-control"
                                    name="department" value="{{ $employee->department }}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            {{-- <label for="version"
                                class="col-md-4 col-form-label text-md-right">{{ __('Version') }}</label> --}}

                            <div class="col-md-6">
                                <input id="version" type="text" class="form-control"
                                    name="version" value="{{ $employee->updated_at }}" hidden>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection