@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ">Employee</div>

                {{-- create new employee --}}
                <div class="card-body">
                    <form method="POST" action="{{ route('employees.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                                    name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address"
                                class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <textarea name="address" id="address"
                                    class="form-control @error('address') is-invalid @enderror" required
                                    autocomplete="address" autofocus></textarea>

                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="department"
                                class="col-md-4 col-form-label text-md-right">{{ __('Department') }}</label>

                            <div class="col-md-6">
                                <select name="department" id="department"
                                    class="form-control @error('department') is-invalid @enderror" required
                                    autocomplete="department" autofocus>
                                    <option value="" selected disabled>Select department</option>
                                    <option value="Information Technology (IT)">Information Technology (IT)</option>
                                    <option value="Human Resource (HR)">Human Resource (HR)</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Technical">Technical</option>
                                    <option value="Customer Service (CS)">Customer Service (CS)</option>
                                    <option value="Support Team">Support Team</option>
                                </select>

                                @error('department')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="text-center">
                            <input type="submit" class="btn btn-success ml-auto" value="Submit">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    {{-- table of emplyee --}}
    <table class="table-striped" style="width: 100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Department</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>
            @if (count($employees)>0)
            @foreach ($employees as $emp)
            <tr>
                <td width="20%">{{$emp->name}}</td>
                <td width="15%">{{$emp->phone}}</td>
                <td width="28%">{{$emp->address}}</td>
                <td width="20%">{{$emp->department}}</td>
                <td width="17%">
                    <div class="row">
                        <a class="btn btn-success" href="{{ route('employees.show', $emp->id) }}">View</a>
                        &nbsp;
                        <form action="{{ route('employees.destroy',$emp->id) }}" method="POST">
                            <a class="btn btn-primary" href="{{ route('employees.edit',$emp->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"
                                onclick="return confirm('Are you sure you want to delete this item?');">Delete</button>
                        </form>
                    </div>
                    
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td class="text-center" colspan="5">No Employee found</td>
            </tr>
            @endif

        </tbody>
    </table>
</div>

@endsection