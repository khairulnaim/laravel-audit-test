@extends('layouts.app')

@section('content')
<div class="container">
    {{-- table of products --}}
    <table class="table table-striped" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Stock</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>
            @if (count($products)>0)
            @foreach ($products as $product)
            <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->stock}}</td>
                <td style="text-align: center">

                    <form action="{{ route('products.update',$product->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-primary"
                            onclick="return confirm('Purchase one item?');">Buy</button>
                    </form>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td class="text-center" colspan="5">No products found</td>
            </tr>
            @endif

        </tbody>
    </table>
</div>

@endsection