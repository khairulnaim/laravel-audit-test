@extends('layouts.app')

@section('content')
<div class="container">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
    <br>
    {{-- create new transfer --}}

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ">Employee</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('transfer') }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="balance"
                                class="col-md-4 col-form-label text-md-right">{{ __('Balance') }}</label>

                            <div class="col-md-6">
                                <input id="balance" type="text"
                                    class="form-control @error('balance') is-invalid @enderror" name="balance"
                                    value="{{ Auth::user()->balance}}" required autocomplete="balance" autofocus
                                    disabled>

                                @error('balance')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="amount"
                                class="col-md-4 col-form-label text-md-right">{{ __('Transfer amount (RM)') }}</label>

                            <div class="col-md-6">
                                <input id="amount" type="number"
                                    class="form-control @error('amount') is-invalid @enderror" name="amount"
                                    value="{{ old('amount') }}" required autocomplete="amount" autofocus>

                                @error('amount')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <input type="text" name="from_user" value="{{Auth::user()->id}}" hidden>
                        <div class="form-group row">
                            <label for="to"
                                class="col-md-4 col-form-label text-md-right">{{ __('Transfer to') }}</label>

                            <div class="col-md-6">
                                <select name="to_user" id="to_user" class="form-control">
                                    <option value="" selected disabled>Select User</option>
                                    @foreach ($users as $user)
                                    @if ($user->id != Auth::user()->id)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endif

                                    @endforeach
                                </select>

                                @error('to')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="text-center">
                            <input type="submit" class="btn btn-success ml-auto" value="Transfer">
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <br><br><br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Amount (RM)</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transactions as $transaction)
                <tr>
                    <td>{{$transaction->id}}</td>
                    <td>{{$transaction->from->name}}</td>
                    <td>{{$transaction->to->name}}</td>
                    <td>{{$transaction->amount}}</td>
                    <td>{{$transaction->updated_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection