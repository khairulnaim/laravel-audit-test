<?php

/*
    Author: Khairul Naim Bin Anuar
    Date & Time Updated: 12:00PM 10/3/2020
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;
use Faker\Provider\fr_FR\Address as FakerDepartment;
use Carbon\Carbon;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->tollFreePhoneNumber,
        'address' => $faker->address,
        'department' => $faker->randomElement(['Information Technology (IT)', 'Human Resource (HR)', 'Admin', 'Technical', 'Customer Service (CS)', 'Support Team']),
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
    ];
});
