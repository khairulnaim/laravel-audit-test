<?php

/*
    Author: Khairul Naim Bin Anuar
    Date & Time Updated: 12:00PM 10/3/2020
*/

use Illuminate\Database\Seeder;
use App\Employee;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create( [
            'id'=>3,
            'name'=>'Dr. Crawford Gibson IV',
            'phone'=>'(888) 413-8712',
            'address'=>'4161 Kale Summit\nArnaldomouth, ME 46566',
            'department'=>'Admin',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>4,
            'name'=>'Alexys Denesik',
            'phone'=>'1-800-437-9742',
            'address'=>'8336 Considine Way\nNew Kathleen, OR 39454-1837',
            'department'=>'Admin',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>5,
            'name'=>'Dr. Hailey Wuckert',
            'phone'=>'1-877-348-7461',
            'address'=>'81754 Morissette Run\nSiennahaven, NY 07153-3298',
            'department'=>'Information Technology (IT)',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>6,
            'name'=>'Frank Tillman',
            'phone'=>'1-855-242-1273',
            'address'=>'57840 Lubowitz Pike Suite 485\nNorth Celestine, KY 34959',
            'department'=>'Support Team',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>7,
            'name'=>'Otis Rau',
            'phone'=>'(877) 463-1504',
            'address'=>'33304 Jazmin Stream\nLake Rosemary, MA 48488-7810',
            'department'=>'Customer Service (CS)',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>8,
            'name'=>'Aglae Stamm',
            'phone'=>'(800) 451-7802',
            'address'=>'206 Donnelly Ferry\nLake Cordelia, KS 22332-3473',
            'department'=>'Information Technology (IT)',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>9,
            'name'=>'Prof. Emery Harris',
            'phone'=>'800.376.2063',
            'address'=>'6977 Jolie Club\nEast Dell, SD 46553',
            'department'=>'Human Resource (HR)',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>2,
            'name'=>'Mr. Ellis Cole',
            'phone'=>'877-896-5997',
            'address'=>'350 Pacocha Stravenue\nMrazland, DE 67276-0536',
            'department'=>'Customer Service (CS)',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>1,
            'name'=>'Laney Crona',
            'phone'=>'1-866-894-9413',
            'address'=>'2545 Cole Meadows Suite 030\nMellieview, CO 98939',
            'department'=>'Information Technology (IT)',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
            
            
                        
            Employee::create( [
            'id'=>10,
            'name'=>'Maximo McCullough',
            'phone'=>'(866) 930-3320',
            'address'=>'82573 Langworth Field Suite 545\nTressaview, RI 60694',
            'department'=>'Support Team',
            'created_at'=>'2020-03-09 19:42:35',
            'updated_at'=>'2020-03-09 19:42:35'
            ] );
    }
}
