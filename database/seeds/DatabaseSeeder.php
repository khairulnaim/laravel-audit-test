<?php

/*
    Author: Khairul Naim Bin Anuar
    Date & Time Updated: 12:00PM 10/3/2020
*/

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmployeesTableSeeder::class);
    }
}
