<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'EmployeeController@index')->name('home');
// Route::resource('employees', 'EmployeeController');
Route::resource('employees', 'EmployeeController')->except(['update']);
Route::put('/employees/{id}/{version}', 'EmployeeController@update')->name('employees.update');
Route::get('/audit', 'AuditController@index');
Route::resource('products', 'ProductsController')->only(['index', 'update']);
Route::resource('transactions', 'TransactionsController')->only(['index']);
Route::put('transactions', 'TransactionsController@transfer')->name('transfer');
