<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('transactions', 'TransactionsController')->only(['index']);
Route::put('transactions', 'TransactionsController@transfer')->name('transfer');
Route::resource('employees', 'EmployeeController')->except(['update']);
Route::put('/employees/{id}/{version}', 'EmployeeController@update')->name('employees.update');
