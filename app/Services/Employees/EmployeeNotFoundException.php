<?php

namespace App\Services\Employees;

class EmployeeNotFoundException extends \Exception
{
}