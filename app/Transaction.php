<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Transaction extends Pivot
{
    protected $table = 'transactions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_user', 'to_user', 'amount',
    ];

    public function from()
    {
        return $this->belongsTo('App\User', 'from_user', 'id', );
    }

    public function to()
    {
        return $this->belongsTo('App\User', 'to_user', 'id');
    }
}
