<?php

/*
Author: Khairul Naim Bin Anuar
Date & Time Updated: 12:06PM 10/3/2020
 */

namespace App\Http\Controllers;

use App\Employee;
use App\Services\Employees\EmployeeNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();

        return view('/home')->with('employees', $employees);
    }

    public function show($id)
    {
        $employee = Employee::whereId($id);

        try {
            if (!$employee->exists()) {
                throw new EmployeeNotFoundException();
            }
        } catch (EmployeeNotFoundException $exception) {
            return view('errors.error404')->with(['error' => 'Employee Not Found.', 'http' => Response::HTTP_NOT_FOUND, 'message' => 'Looks like someone made a mess!']);
        }

        $departments = ['Information Technology (IT)', 'Human Resource (HR)', 'Admin', 'Technical', 'Customer Service (CS)', 'Support Team'];

        return view('employees.show', ['employee' => $employee->first(), 'departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Employee;

        $employee->name = $request->input('name');
        $employee->phone = $request->input('phone');
        $employee->address = $request->input('address');
        $employee->department = $request->input('department');
        $employee->save();

        return redirect()->route('employees.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $departments = ['Information Technology (IT)', 'Human Resource (HR)', 'Admin', 'Technical', 'Customer Service (CS)', 'Support Team'];

        return view('edit', ['employee' => $employee, 'departments' => $departments]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $version)
    {

        try {
            DB::beginTransaction();

            sleep(1);
            $employee = Employee::whereId($id)
                                ->where('updated_at', '=', $version)
                                ->update([
                                    'name' => $request->input('name'),
                                    'phone' => $request->input('phone'),
                                    'address' => $request->input('address'),
                                    'department' => $request->input('department')
                                ]);

            if(!$employee) {
                throw new IncorrectRecordVersionException();
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
            // return view('errors.error404')->with(['error' => 'Incorrect Update Version.', 'http' => '400', 'message' => 'Employee record must have been tampered with.']);
        }

        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employees.index');
    }
}
