<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products')->with(['products' => $products]);
    }

    public function update(Product $product)
    {

        DB::beginTransaction();

        try {

            $product->stock -= 1;
            $product->save();

            sleep(30);

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return redirect()->route('products.index');
    }
}
