<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use DB;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        // $from_users = DB::table('transactions')
        //     ->join('users', 'users.id', '=', 'transactions.from_user')->orderBy('transactions.created_at', 'desc')->get();
        // $to_users = DB::table('transactions')
        //     ->join('users', 'users.id', '=', 'transactions.to_user')->orderBy('transactions.created_at', 'desc')->get();

        $transactions = Transaction::with('from', 'to')->orderBy('created_at', 'desc')->get();
        // dd($transactions);
        return view('transfers')->with(['users' => $users, 'transactions' => $transactions]);
        // return view('transfers')->with(['users' => $users, 'from_users' => $from_users, 'to_users' => $to_users]);
    }

    public function transfer(Request $request)
    {
        DB::beginTransaction();

        try {

            $fromQuery = User::whereId($request->input('from_user'));

            $toQuery = User::whereId($request->input('to_user'));

            $fromAccount = $fromQuery->lockForUpdate()->first();

            $toAccount = $toQuery->lockForUpdate()->first();

            $toAccount->balance += $request->input('amount');
            $toAccount->save();

            $fromAccount->balance -= $request->input('amount');
            $fromAccount->save();

            $transaction = new Transaction();
            $transaction->from_user = $request->input('from_user');
            $transaction->to_user = $request->input('to_user');
            $transaction->amount = $request->input('amount');
            $transaction->save();

            DB::commit();

            return redirect()->route('employees.index');

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
